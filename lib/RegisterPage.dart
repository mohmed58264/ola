import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:SweetApp/MainPage.dart';
class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  bool progressSave = false;
  String birthday = 'تاريخ الميلاد';
  TextEditingController txtPhone = new TextEditingController();
  String countryKey;

  bool progress = false;
  List<String> country = [
    "SA" /*, "BH", "KW", "AE", "OM", "QA", "EG", "JO"*/
  ];

  @override
  Widget build(BuildContext context) {

    return Scaffold(backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(child: SizedBox(
                  child: ClipOval(
                    child: CachedNetworkImage(
                      imageUrl: 'https://camp-mars.com//assets/img/admin.png',
                      placeholder: (context, url) =>
                      new CircularProgressIndicator(
                        backgroundColor: Colors.white,
                      ),
                      errorWidget: (context, url, error) =>
                      new Icon(
                        Icons.account_circle,
                        size: 75,
                        color: Colors.white,
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
                  width: 120,
                  height: 120,
                ),margin: EdgeInsets.only(top: 70),),
                SizedBox(
                  height: 15,
                ),
              ],
            ),

            Container(
                margin: EdgeInsets.only(top: 50),
                child: Column(
                  children: <Widget>[
                    Container(
                      child: Text(
                        "قم بادخال البيانات   ",
                        style: TextStyle(fontSize: 20,color: Theme.of(context).primaryColor),
                      ),
                      margin: EdgeInsets.all(10),
                    ),


                    Container(
                      padding: EdgeInsets.fromLTRB(1, 10, 1, 10),
                      child: SizedBox(
                        child: TextField(
                            controller: txtPhone,
                            keyboardType: TextInputType.phone,
                            decoration: new InputDecoration(
                              counterText: "",
                              labelText: " اسم المستخدم",
                              enabledBorder: OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(60.0),
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 0.9),
                              ),
                              border: const OutlineInputBorder(
                                  borderSide: const BorderSide(
                                      color: Colors.grey, width: 0.9)),
                            )),
                        height: 50,
                      ),
                      margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(1, 10, 1, 10),
                      child: SizedBox(
                        child: TextField(
                            controller: txtPhone,
                            keyboardType: TextInputType.phone,
                            decoration: new InputDecoration(
                              counterText: "",
                              labelText: " كلمة المرور ",
                              enabledBorder: OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(60.0),
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 0.9),
                              ),
                              border: const OutlineInputBorder(
                                  borderSide: const BorderSide(
                                      color: Colors.grey, width: 0.9)),
                            )),
                        height: 50,
                      ),
                      margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    ),

                    Container(
                      padding: EdgeInsets.fromLTRB(1, 10, 1, 10),
                      child: SizedBox(
                        child: TextField(
                            controller: txtPhone,
                            keyboardType: TextInputType.phone,
                            decoration: new InputDecoration(
                              counterText: "",
                              labelText: "تكرار كلمة المرور ",
                              enabledBorder: OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(60.0),
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 0.9),
                              ),
                              border: const OutlineInputBorder(
                                  borderSide: const BorderSide(
                                      color: Colors.grey, width: 0.9)),
                            )),
                        height: 50,
                      ),
                      margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    ),

                    Container(
                      padding: EdgeInsets.fromLTRB(1, 10, 1, 10),
                      child: SizedBox(
                        child: TextField(
                            controller: txtPhone,
                            keyboardType: TextInputType.phone,
                            decoration: new InputDecoration(
                              counterText: "",
                              labelText: "  الايميل ",
                              enabledBorder: OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(60.0),
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 0.9),
                              ),
                              border: const OutlineInputBorder(
                                  borderSide: const BorderSide(
                                      color: Colors.grey, width: 0.9)),
                            )),
                        height: 50,
                      ),
                      margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    ),


                    Container(
                      padding: EdgeInsets.fromLTRB(1, 10, 1, 10),
                      child: SizedBox(
                        child: TextField(
                            controller: txtPhone,
                            keyboardType: TextInputType.phone,
                            decoration: new InputDecoration(
                              counterText: "",
                              labelText: " العنوان  ",
                              enabledBorder: OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(60.0),
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 0.9),
                              ),
                              border: const OutlineInputBorder(
                                  borderSide: const BorderSide(
                                      color: Colors.grey, width: 0.9)),
                            )),
                        height: 50,
                      ),
                      margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    ),


                    Container(
                      child: SizedBox(
                        child: RaisedButton(
                            child: progressSave
                                ? Center(
                              child: CircularProgressIndicator(
                                backgroundColor: Colors.white,
                              ),
                            )
                                : Text(
                              "تسجيل الدخول",
                              style: TextStyle(
                                  fontSize: 14, color: Colors.white),
                            ),
                            padding: EdgeInsets.fromLTRB(89, 0, 89, 0),
                            onPressed: ()=>{
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => MainPage(),
                                  ))

                            },
                            color: Theme.of(context).primaryColor,
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(60.0))),
                        height: 40,
                      ),
                      margin: EdgeInsets.fromLTRB(0, 20, 0, 50),
                    ),


                  ],
                )),
          ],
        ),
      ),
    );
  }
  void _launchURL(_url) async =>
      await canLaunch(_url) ? await launch(_url) : throw 'Could not launch $_url';


}
