import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:SweetApp/MainPage.dart';
import 'package:SweetApp/RegisterPage.dart';
class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool progressSave = false;
  String birthday = 'تاريخ الميلاد';
  TextEditingController txtPhone = new TextEditingController();
  String countryKey;

  bool progress = false;
  List<String> country = [
    "SA" /*, "BH", "KW", "AE", "OM", "QA", "EG", "JO"*/
  ];

  @override
  Widget build(BuildContext context) {

    return Scaffold(backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          children: <Widget>[

            Container(
                margin: EdgeInsets.only(top: 50),
                child: Column(
                  children: <Widget>[
                    Container(
                      child: SizedBox(
                        child: RaisedButton(
                            child: progressSave
                                ? Center(
                              child: CircularProgressIndicator(
                                backgroundColor: Colors.white,
                              ),
                            )
                                : Text(
                              "معرفة سعر المنتج",
                              style: TextStyle(
                                  fontSize: 14, color: Colors.white),
                            ),
                            padding: EdgeInsets.fromLTRB(89, 0, 89, 0),
                            onPressed: ()=>{
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => RegisterPage(),
                                  ))

                            },
                            color: Theme.of(context).primaryColor,
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(60.0))),
                        height: 100,
                      ),
                      margin: EdgeInsets.fromLTRB(0, 5, 0, 50),
                    ),



                    Container(
                      child: SizedBox(
                        child: RaisedButton(
                            child: progressSave
                                ? Center(
                              child: CircularProgressIndicator(
                                backgroundColor: Colors.white,
                              ),
                            )
                                : Text(
                              "بلاغ لمحل تموينات",
                              style: TextStyle(
                                  fontSize: 14, color: Colors.white),
                            ),
                            padding: EdgeInsets.fromLTRB(89, 0, 89, 0),
                            onPressed: ()=>{
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => RegisterPage(),
                                  ))

                            },
                            color: Theme.of(context).primaryColor,
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(60.0))),
                        height: 100,
                      ),
                      margin: EdgeInsets.fromLTRB(0, 5, 0, 50),
                    ),


                    Container(
                      child: SizedBox(
                        child: RaisedButton(
                            child: progressSave
                                ? Center(
                              child: CircularProgressIndicator(
                                backgroundColor: Colors.white,
                              ),
                            )
                                : Text(
                              "بلاغ محلية",
                              style: TextStyle(
                                  fontSize: 14, color: Colors.white),
                            ),
                            padding: EdgeInsets.fromLTRB(89, 0, 89, 0),
                            onPressed: ()=>{
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => RegisterPage(),
                                  ))

                            },
                            color: Theme.of(context).primaryColor,
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(60.0))),
                        height: 100,
                      ),
                      margin: EdgeInsets.fromLTRB(0, 5, 0, 50),
                    ),

                  ],
                )),
          ],
        ),
      ),
    );
  }
  void _launchURL(_url) async =>
      await canLaunch(_url) ? await launch(_url) : throw 'Could not launch $_url';


}
