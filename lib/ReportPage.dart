import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class ReportPage extends StatefulWidget {
  @override
  _ReportPageState createState() => _ReportPageState();
}

class _ReportPageState extends State<ReportPage> {
  bool progress = false;
  List<ProductItem> favorites = new List<ProductItem> ();

  @override
  void initState() {
    super.initState();
    favorites.add(new ProductItem(
        arabicName: " 08/08/2021",
        boxPrice: 150,
        category: null,
        categoryId: 1,onePiecePrice: 75,
        colariesDescriptoin: "",
        createdOn: '',itemImagePath: 'https://upload.wikimedia.org/wikipedia/commons/6/67/%D8%B9%D8%AF%D8%B3_%D8%A7%D9%84%D9%81%D8%B1%D8%A7%D8%B4%D8%A9.png',
        description: '',englishName: '',id: 1,isFitWithEndUser: false,isFitWithMerchant: false,isFitWithSchool: false,rate: 8));

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: progress
          ? Center(
        child: CircularProgressIndicator(),
      )
          : favorites.length == 0
          ? Center(
        child: Text("لا توجد عناصر في المفضلة"),
      )
          : Stack(
        children: [
          Container(
            margin: EdgeInsets.only(right: 10, left: 10, top: 50),
            child: ListView.separated(
              itemBuilder: (context, index) {
                return GestureDetector(
                  child: Container(
                      width: double.infinity,
                      alignment: Alignment.topCenter,
                      child: Card(
                        shape: new RoundedRectangleBorder(
                            borderRadius:
                            new BorderRadius.circular(10.0)),
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          children: [

                            Container(
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.end,
                                mainAxisAlignment:
                                MainAxisAlignment.end,
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  Align(
                                    child: Column(
                                      children: [
                                        Row(
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(
                                                  right: 10,
                                                  left: 10,
                                                  top: 20,
                                                  bottom: 10),
                                              child: Text(
                                                '150',
                                                style: TextStyle(
                                                    fontSize: 20),
                                              ),
                                              alignment:
                                              Alignment.topCenter,
                                            ),
                                            Spacer(),
                                            Container(
                                              margin: EdgeInsets.only(
                                                  right: 10,
                                                  left: 10,
                                                  top: 20,
                                                  bottom: 10),
                                              child: Text('رقم البلاغ' ),
                                              alignment:
                                              Alignment.topCenter,
                                            ),
                                          ],
                                        ),

                                        Row(
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(
                                                  right: 10,
                                                  left: 10,
                                                  top: 20,
                                                  bottom: 10),
                                              child: Text(
                                                favorites
                                                    .elementAt(index)
                                                    .arabicName,
                                                style: TextStyle(
                                                    fontSize: 20),
                                              ),
                                              alignment:
                                              Alignment.topCenter,
                                            ),
                                            Spacer(),
                                            Container(
                                              margin: EdgeInsets.only(
                                                  right: 10,
                                                  left: 10,
                                                  top: 20,
                                                  bottom: 10),
                                              child: Text('تاريخ البلاغ' ),
                                              alignment:
                                              Alignment.topCenter,
                                            ),
                                          ],
                                        ),

                                        Row(
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(
                                                  right: 10,
                                                  left: 10,
                                                  top: 20,
                                                  bottom: 10),
                                              child: Text(
                                                'السيارة  في الطريق',
                                                style: TextStyle(
                                                    fontSize: 20),
                                              ),
                                              alignment:
                                              Alignment.topCenter,
                                            ),
                                            Spacer(),
                                            Container(
                                              margin: EdgeInsets.only(
                                                  right: 10,
                                                  left: 10,
                                                  top: 20,
                                                  bottom: 10),
                                              child: Text('حالة البلاغ' ),
                                              alignment:
                                              Alignment.topCenter,
                                            ),
                                          ],
                                        ),

                                        Row(
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(
                                                  right: 10,
                                                  left: 10,
                                                  top: 20,
                                                  bottom: 10),
                                              child: Text(
                                               'ضغط كهربائي علي في منطقة الكلاكلة',
                                                style: TextStyle(
                                                    fontSize: 20),
                                              ),
                                              alignment:
                                              Alignment.topCenter,
                                            ),
                                            Spacer(),
                                            Container(
                                              margin: EdgeInsets.only(
                                                  right: 10,
                                                  left: 10,
                                                  top: 20,
                                                  bottom: 10),
                                              child: Text('وصف البلاغ' ),
                                              alignment:
                                              Alignment.topCenter,
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    alignment: Alignment.topCenter,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )),
                  onTap: () {
                    // Navigator.push(
                    //     context,
                    //     MaterialPageRoute(
                    //       builder: (context) =>
                    //           ProductDetails(
                    //               favorites
                    //                   .elementAt(index),
                    //               Theme.of(context).primaryColor),
                    //     ));
                    //
                  },
                );
              },
              scrollDirection: Axis.vertical,
              separatorBuilder: (context, index) {
                return SizedBox(
                  width: 0,
                );
              },
              itemCount: favorites.length,
              shrinkWrap: true,
            ),
          )
        ],
      ),
    );
  }
}

class ProductItem {
  int id;
  String arabicName;
  String englishName;
  String createdOn;
  String itemImagePath;
  Null category;
  int categoryId;
  double onePiecePrice;
  double boxPrice;
  double packagePrice;
  bool isImportedItem;
  bool isFitWithSchool;
  bool isFitWithEndUser;
  bool isFitWithMerchant;
  String description;
  String colariesDescriptoin;
  Null orderItems;
  bool isInFavourite;
  int rate;

  ProductItem(
      {this.id,
        this.arabicName,
        this.englishName,
        this.createdOn,
        this.itemImagePath,
        this.category,
        this.categoryId,
        this.onePiecePrice,
        this.boxPrice,
        this.packagePrice,
        this.isImportedItem,
        this.isFitWithSchool,
        this.isFitWithEndUser,
        this.isFitWithMerchant,
        this.description,
        this.colariesDescriptoin,
        this.orderItems,
        this.isInFavourite,
        this.rate});

  ProductItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    arabicName = json['arabicName'];
    englishName = json['englishName'];
    createdOn = json['createdOn'];
    itemImagePath = json['itemImagePath'];
    category = json['category'];
    categoryId = json['categoryId'];
    onePiecePrice = json['onePiecePrice'];
    boxPrice = json['boxPrice'];
    packagePrice = json['packagePrice'];
    isImportedItem = json['isImportedItem'];
    isFitWithSchool = json['isFitWithSchool'];
    isFitWithEndUser = json['isFitWithEndUser'];
    isFitWithMerchant = json['isFitWithMerchant'];
    description = json['description'];
    colariesDescriptoin = json['colariesDescriptoin'];
    orderItems = json['orderItems'];
    isInFavourite = json['isInFavourite'];
    rate = json['rate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['arabicName'] = this.arabicName;
    data['englishName'] = this.englishName;
    data['createdOn'] = this.createdOn;
    data['itemImagePath'] = this.itemImagePath;
    data['category'] = this.category;
    data['categoryId'] = this.categoryId;
    data['onePiecePrice'] = this.onePiecePrice;
    data['boxPrice'] = this.boxPrice;
    data['packagePrice'] = this.packagePrice;
    data['isImportedItem'] = this.isImportedItem;
    data['isFitWithSchool'] = this.isFitWithSchool;
    data['isFitWithEndUser'] = this.isFitWithEndUser;
    data['isFitWithMerchant'] = this.isFitWithMerchant;
    data['description'] = this.description;
    data['colariesDescriptoin'] = this.colariesDescriptoin;
    data['orderItems'] = this.orderItems;
    data['isInFavourite'] = this.isInFavourite;
    data['rate'] = this.rate;
    return data;
  }
}
